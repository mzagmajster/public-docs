# Public documemtation

Documetation for my public projects.

## Getting Started

### Prerequisites

To be able to use this project Python has to be installed on machine

### Installing

Clone project.

For python projects sphinx is used for generating documentation. In order to be able to use documentation for python projects the following command should be executed.

```
pip install sphinx sphinx_rtd_theme
```

## Deployment

On server level map correct folder.

## Contributing

Please read ```CONTRIBUTING.md``` for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

Initial content for this project was provided by Matic Zagmajster. For more information please see [AUTHORS]() file.

## License

This project does not a license, please refer to ```LICENSE``` file for details.
