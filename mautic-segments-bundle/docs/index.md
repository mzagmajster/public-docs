# MZagmajsterSegmentsBundle for Mautic 4

**Prioritize segment execution by using a queue.** 

## Setup

**Install Redis Server:** Install Redis Server (version >= 7.0) by following this [guide](https://redis.io/docs/install/install-redis/install-redis-on-linux/).

**Install the plugin:**

```bash
cd <mautic-root>
cp -R <plugin-source> /plugins/MZagmajsterSegmentsBundle
rm -rf var/cache/prod/*
php bin/console mautic:plugins:install --env=prod
```

**Configure Default Group and Access to Redis Server**

To set up essential parameters for the plugin, copy the snippet below and paste it inside ```<mautic-root>/app/config/local.php```:

```php
'mzs_leadlist_default_group' => 'MINUTELY',
'mzs_redis'                  => [
    'host'      => '<host>',
    'port'      => 6379,
    'database'  => 0,
],
```

Option: ```mzs_leadlist_default_group``` specifies the default group to which a segment should be moved upon creation.

**Install the script for segment updates using Redis:**

```bash
cp MZagmajsterSegmentsBundle/plugins/bin/mzs_redis_segments_update.sh /usr/local/bin
```

- Open `/usr/local/bin/mzs_redis_segments_update.sh`
- Customize the following variables:

```bash
REDIS_HOST
REDIS_DB
REDIS_PORT
```

- Ensure the correct PHP executable and Mautic path are used in the script. Save the script.

**Initialize lead list rules by moving all the segments to one group at the beginning.**

```bash
php bin/console doctrine:query:sql "INSERT INTO mzs_lead_list_rules (leadlist_id, cron_group)
SELECT id, 'MINUTELY'
FROM lead_lists; "
```
- Then, update the group for specific segments that you want in a different group. In the example above, we initially move all segments to the 'MINUTELY' group. In the example below, we move segments 1, 2, 3 to the 'DAILY' group.

```bash
php bin/console doctrine:query:sql "UPDATE mzs_lead_list_rules SET cron_group = 'DAILY' WHERE leadlist_id IN (1,2,3); "
```

**Initialize Redis database:**

```bash
php bin/console mzs:queue:init --group=<GROUP NAME>
```

## Plugin Commands

- **Initialize queue segments with a specific group:**

```bash
php bin/console mzs:queue:init --group=<GROUP NAME>
```

- **Move a segment up the queue:**

```bash
php bin/console mzs:queue:move --list-id=<X>
```

## Things to Be Aware Of

- A segment needs to be published to be added to the queue.
- When the plugin is active and you create a new published segment, it automatically moves the segment to the top.
- When you unpublish the segment, run `php bin/console mzs:queue:init` to remove the segment from the queue.
- You only need one script: ```mzs_redis_segments_update.sh``` per Mautic instance, but each script needs to point to different Redis database.

## Typical Usage

Suppose you have segments nicely grouped into categories like MINUTELY and DAILY, for example. At the server level, configure the system to trigger the reinitialization of the queue, enabling the processing of new segment groups. This can be achieved through cron or systemd. Below is an example of how such a timer might be configured.

/etc/systemd/system/example_mzs_group_daily.timer:


```
[Unit]
Description=daily_timer

[Timer]
OnCalendar=*-*-* 23:00:00
Persistent=false

[Install]
WantedBy=timers.target
```

/etc/systemd/system/example_mzs_group_daily.service:

```
[Unit]
Description=example_mzs_group

[Service]
Type=oneshot
User=www-data
Group=www-data
ExecStart=/usr/bin/php /var/www/html/mautic/bin console mzs:queue:init --group=DAILY

[Install]
WantedBy=multi-user.target
```

And install one timer and service for each group. **Please be aware that inside the timers, we are referring to server time, which may differ from your local time. Therefore, consider this when setting up these configurations on the server.**

## Contact

Should you have any issues, do not hesitate to reach out and we will try to assist you.

* Email: maticzagmajster[ at ]gmail[ dot ]com (fastest)
* [Mautic Forums](https://forum.mautic.org/)
* [Mautic Slack](https://www.mautic.org/slack)
* [Website](https://maticzagmajster.ddns.net/)