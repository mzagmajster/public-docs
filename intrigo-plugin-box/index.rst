.. RefineIt Plugin documentation master file, created by
   sphinx-quickstart on Sun Apr 14 02:34:35 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Intrigo Plugin Box's documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   basic



Indices and tables
==================

* `API documentation <http://maticzagmajster.ddns.net/docs/intrigo-plugin-box/phpdox/docs/html>`_.
