Basic
********

Plugin activation
===================

The plugin base module (**RefineIt**) introduces action for activation called **refine_it_module_activation**. This name comes from the module prefix (**refine_it_**) and main name (**module_activation**).

For each new module, new (additional) action hook is created and used.

This hook is used inside ``\RefineIt\Support\activation`` function and connects ``activation`` method from each ``PluginShell`` class.

This implementation of module activation mechanism makes it possible to start implementing the logic for plugin activation right away. We also have a consistent interface (the logic for activation will always be in ``activation`` method).

Plugin deactivation
====================

The plugin base module (**RefineIt**) introduces action for deactivation called **refine_it_module_deactivation**. This name comes from the module prefix (**refine_it_**) and main name (**module_deactivation**).

For each new module, new (additional) action hook is created and used.

This hook is used inside ``\RefineIt\Support\deactivation`` function and connects ``deactivation`` method from each ``PluginShell`` class.

This implementation of module deactivation mechanism makes it possible to start implementing the logic for plugin deactivation right away. We also have a consistent interface (the logic for deactivation will always be in ``deactivation`` method).

.. note:: If you use the **RefineIt** base the right way, it will speed up the implementation and simplify your life.