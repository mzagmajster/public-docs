Configuration object
**********************

We are adding configuration options as needed. Configuration object for MinSplinter class is basically dictionary of options that are passed to the underlying Splinter class.

Currently MinSplinter class excepts options documented below.


* \ **DRIVER_NAME**\ - Which web driver should be used by class. *Default*\: 'firefox'

* \ **USER_AGENT**\ - Specify user-agent to use. \ *Default*\: None

* \ **PROFILE**\ - Absolute path to profile folder of selected web driver (browser). Available when using 'firefox' web driver.
